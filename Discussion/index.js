

// CRUD Operations

// Are the heart of any backend application
// CRUD stands for Create, Read/Retrieve, Update, Delete
// MongoDB deals with objects as its structure for documents, we can easily create by providing objects into out method

// Create: Inserting documents

// Insert One:

// Syntax:
	// db.collectionName.insertOne({object});

// Inserting/ Assigning values in JS Objects:
	// object.object.method({object})


db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "576850",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","Javascript","Python"],
	department: "none"
});

// Insert Many

// Syntax:
	// db.users.insertMany([{objectA}, {objectB}]);

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5656544",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 76,
		contact: {
			phone: "4643434",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
]);

// Reading: Finding Documents

// Find All

// Syntax:
	// db.collectionName.find();

db.users.find();

// Finding users with signle arguments

// Syntax:
	// db.collectionName.find({field:value})

db.users.find({firstName: "Stephen"});


// Fetched 0 record/s
db.users.find({ firstName: "Stephen", age: 20});

// Key found
db.users.find({ firstName: "Stephen", age: 76});


// Update: Updating Documents

	// Repeat Jane to be updated

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "576850",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","Javascript","Python"],
	department: "none"
});

// Update One

// Syntax:
	// db.collectionname.updateOne({criteria}, {$set: (field:value)});


db.users.updateOne(
	{firstName: "Jane"},
	{
		$set: {
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "576851",
				email: "janegates@gmail.com"
			},
			courses: ["AWS","Google Cloud","Azure"],
			department: "infrastructure",
			status: "active"
				}
			}
);

db.users.find({firstName: "Jane"});

// Update Many

db.users.updateMany(
	{department: "none"},
	{
		$set: 
		{
			department: "HR"
		}
	}
)
db.users.find().pretty();


// Replace One

// Can be used if replacing the whole document is necessary

// Syntax: 
	// db.collectionName.replaceOne( {criteria}, {field: value});

db.users.replaceOne(
	{ lastName: "Gates"},
	{
			firstName: "Bill",
			lastName: "Clinton"
	}	
	);
db.users.find({firstName: "Bill"});


// Delete: Deleting Documents

db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	course: [],
	department: "none"
});
db.users.find({firstName: "Test"});

// Deleting a single document

// Syntax: 
	// db.collectionName.deleteOne({criteria})

db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});

db.users.deleteOne({age: 76});
db.users.find();

// Delete Many

db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	course: [],
	department: "none"
});
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	course: [],
	department: "none"
});

db..users.deleteMany({
	courses: []
});

db.users.find();

// Delete all

// db.users.delete()

// Be careful when using the deleteMany method. If no search criteria is provided. It will delete all documents

// DO NOT USE: db.collection.deleteMany()

// Syntax:
	// db.collectionname.deleteMany({criteria})



// Advanced Queries

db.users.find(
{
	contact: {
		phone: "576850",
		email: "janedoe@gmail.com"
	}
});

// Find the document with the email "janedoe@gmail.com"

db.users.find({"contact.email": "janedoe@gmail.com"});


// Querying an Array with exact elements

// Querying with an exact array
db.users.find({courses: [ 
        "React", 
        "Laravel", 
        "SASS"
    ]});

// Querying without an exact array
db.users.find({courses: {$all: [ 
        "SASS", 
        "React", 
        "Laravel"
    ]}});

// Make an array to query

db.users.insert({
	nameArr: [
	{
		nameA: "Juan"
	},
	{
		nameB: "tamad"
	}
	]
});

db.users.find({
	nameArr: {
		nameA: "Juan"
	}
});